(defun figo(a)
	(if(>= a 1)
		(progn
			(print 1)	
			(figo2 a 1 1)
		)
	)
)
	
(defun figo2 (a b c)	
	(if(<= c a)
		(progn
			(print c)	
			(figo2 a c (+ b c))
		)
	)
)