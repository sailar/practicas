

(defun d1()

(flet (
	(Atriangulo(b a) 
		(/(* a b) 2)
	)
		)
	(Atriangulo 5 7)
)
)



(defun d2()
(flet (
	(Acuadrado(a) 
(* a a)
	)
		)
	(Acuadrado 5)
)
)



(defun d3()
(flet (
	(Arectangulo(a b) 
(* a b)
	)
		)
	(Arectangulo 5 3)
)
)




(defun d4()
(flet (
	(Atrapecio(a b c) 
	(/(*(* a b)c)2)
	)
		)
	(Atrapecio 5 8 9)
)
)




(defun d5()
(flet (
	(Aparalelogramo(a b) 
(* a b)
	)	
		)
	(Aparalelogramo 5 2)
)
)





(defun d6()
(flet (
	(Vprismatriangular(b a c) 
(*(/(* a b) 2)c)
	)
		)
	(Vprismatriangular 5 7 8)
)
)





(defun d7()
(flet (
	(Vcubo(a) 
(* a a a)
	)
		)
	(Vcubo 5)
)
)





(defun d8()
(flet (
(Vprismarectangular(a b c) 
(* a b c)
	)	
		)
	(Vprismarectangular 5 3 2)
)
)




(defun d9()
(flet (
	(Vprismatrapecio(a b c n) 
(*(/(*(* a b)c)2)n)
	)
		)
	(Vprismatrapecio 5 7 8 9)
)
)


(defun d10()
(flet (
	(Vprismaparalelogramo(a b c) 
(* a b c)
	)
		)
	(Vprismaparalelogramo 5 7 9)
)
)


