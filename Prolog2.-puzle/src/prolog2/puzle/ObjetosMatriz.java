/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prolog2.puzle;

/**
 *
 * @author saila
 */


public class ObjetosMatriz {
private int Azulx=3;
private int Azuly=1;

private int Verdex=1;
private int Verdey=1;

private int RojoCx=2;
private int RojoCy=0;

private int RojoUx=2;
private int RojoUy=1;

    public int getAzulx() {
        return Azulx;
    }

    public void setAzulx(int Azulx) {
        this.Azulx = Azulx;
    }

    public int getAzuly() {
        return Azuly;
    }

    public void setAzuly(int Azuly) {
        this.Azuly = Azuly;
    }

    public int getVerdex() {
        return Verdex;
    }

    public void setVerdex(int Verdex) {
        this.Verdex = Verdex;
    }

    public int getVerdey() {
        return Verdey;
    }

    public void setVerdey(int Verdey) {
        this.Verdey = Verdey;
    }

    public int getRojoCx() {
        return RojoCx;
    }

    public void setRojoCx(int RojoCx) {
        this.RojoCx = RojoCx;
    }

    public int getRojoCy() {
        return RojoCy;
    }

    public void setRojoCy(int RojoCy) {
        this.RojoCy = RojoCy;
    }

    public int getRojoUx() {
        return RojoUx;
    }

    public void setRojoUx(int RojoUx) {
        this.RojoUx = RojoUx;
    }

    public int getRojoUy() {
        return RojoUy;
    }

    public void setRojoUy(int RojoUy) {
        this.RojoUy = RojoUy;
    }

}
