
package prolog3.cadenaslaberinto;



public class matrizLab {
    
    private String matrizLaberinto[][]=new String [6][6];
    
    matrizLab(){
        
        matrizLaberinto[0][0]="b";
        matrizLaberinto[0][1]="b";
        matrizLaberinto[0][2]="3";
        matrizLaberinto[0][3]="3";
        matrizLaberinto[0][4]="3";
        matrizLaberinto[0][5]="3";
        matrizLaberinto[1][0]="1";
        matrizLaberinto[1][1]="1";
        matrizLaberinto[1][2]="1";
        matrizLaberinto[1][3]="b";
        matrizLaberinto[1][4]="b";
        matrizLaberinto[1][5]="3";
        matrizLaberinto[2][0]="";
        matrizLaberinto[2][1]="b";
        matrizLaberinto[2][2]="";
        matrizLaberinto[2][3]="b";
        matrizLaberinto[2][4]="b";
        matrizLaberinto[2][5]="3";
        matrizLaberinto[3][0]="2";
        matrizLaberinto[3][1]="2";
        matrizLaberinto[3][2]="";
        matrizLaberinto[3][3]="3";
        matrizLaberinto[3][4]="3";
        matrizLaberinto[3][5]="3";
        matrizLaberinto[4][0]="b";
        matrizLaberinto[4][1]="2";
        matrizLaberinto[4][2]="b";
        matrizLaberinto[4][3]="3";
        matrizLaberinto[4][4]="b";
        matrizLaberinto[4][5]="b";
        matrizLaberinto[5][0]="2";
        matrizLaberinto[5][1]="2";
        matrizLaberinto[5][2]="";
        matrizLaberinto[5][3]="3";
        matrizLaberinto[5][4]="3";
        matrizLaberinto[5][5]="";
        
        
        
    }
    
    public String[][] getMatrizLaberinto() {
        return matrizLaberinto;
    }
    /**
    * @param matrizLaberinto, es la matris donde se simulan los movimientos.
    */
    public void setMatrizLaberinto(String[][] matrizLaberinto) {
        this.matrizLaberinto = matrizLaberinto;
    }
    
}
