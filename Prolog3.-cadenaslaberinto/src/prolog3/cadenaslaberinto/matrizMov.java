/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prolog3.cadenaslaberinto;

/**
 *
 * @author saila
 */
public class matrizMov {
    /**
     * creo el objeto de matriz para llamarla y empiezo a llamar los pasos y imprimir
     * @param matrizLab, clase de la matriz
     */
    /**
     * @
     * @param copiaMatriz[][], donde se guarda la matriz que se obtiene del objeto
     */
    void inicioMov(){
        matrizLab mL=new matrizLab();
        String copiaMatriz[][] =mL.getMatrizLaberinto();
        imprimir(copiaMatriz);
        copiaMatriz=primerPaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=segundoPaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=tercerPaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=cuatroPaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=cincoPaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=seisPaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=sietePaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=sieteSegundoPaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=ochoPaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=nuevePaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=diezPaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=oncePaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=docePaso(copiaMatriz);
        imprimir(copiaMatriz);
        copiaMatriz=trecePaso(copiaMatriz);
        imprimir(copiaMatriz);
        System.out.println("Ganaste XD");
    }
    /**
     * @param copiaMatriz se recive la copia de la matriz para ser utilizada
     */
    String[][] primerPaso(String copiaMatriz[][]){
        copiaMatriz[2][0]="2";
        copiaMatriz[3][0]="2";
        copiaMatriz[3][1]="2";
        copiaMatriz[4][1]="2";
        copiaMatriz[5][1]="2";
        copiaMatriz[5][0]="";
        return copiaMatriz;
    }
    /**
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] segundoPaso(String copiaMatriz[][]){
        copiaMatriz[2][0]="";
        copiaMatriz[3][0]="2";
        copiaMatriz[3][1]="2";
        copiaMatriz[4][1]="2";
        copiaMatriz[5][1]="2";
        copiaMatriz[5][2]="2";
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] tercerPaso(String copiaMatriz[][]){
        copiaMatriz[2][0]="1";
        copiaMatriz[1][0]="1";
        copiaMatriz[1][1]="1";
        copiaMatriz[1][2]="";
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] cuatroPaso(String copiaMatriz[][]){
        copiaMatriz[2][2]="3";
        copiaMatriz[1][2]="3";
        copiaMatriz[0][2]="3";
        copiaMatriz[0][3]="3";
        copiaMatriz[0][4]="3";
        copiaMatriz[0][5]="3";
        copiaMatriz[1][5]="3";
        copiaMatriz[2][5]="3";
        copiaMatriz[3][5]="3";
        copiaMatriz[3][4]="3";
        copiaMatriz[3][3]="3";
        copiaMatriz[4][3]="3";
        copiaMatriz[5][3]="";
        copiaMatriz[5][4]="";
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] cincoPaso(String copiaMatriz[][]){
        copiaMatriz[3][0]="";
        copiaMatriz[3][1]="2";
        copiaMatriz[4][1]="2";
        copiaMatriz[5][1]="2";
        copiaMatriz[5][2]="2";
        copiaMatriz[5][3]="2";
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] seisPaso(String copiaMatriz[][]){
        copiaMatriz[3][2]="2";
        copiaMatriz[3][1]="2";
        copiaMatriz[4][1]="2";
        copiaMatriz[5][1]="2";
        copiaMatriz[5][2]="2";
        copiaMatriz[5][3]="";
        copiaMatriz[1][1]="";
        copiaMatriz[1][0]="1";
        copiaMatriz[2][0]="1";
        copiaMatriz[3][0]="1";
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] sietePaso(String copiaMatriz[][]){
        copiaMatriz[2][2]="";
        copiaMatriz[5][3]="3";
        copiaMatriz[1][2]="3";
        copiaMatriz[0][2]="3";
        copiaMatriz[0][3]="3";
        copiaMatriz[0][4]="3";
        copiaMatriz[0][5]="3";
        copiaMatriz[1][5]="3";
        copiaMatriz[2][5]="3";
        copiaMatriz[3][5]="3";
        copiaMatriz[3][4]="3";
        copiaMatriz[3][3]="3";
        copiaMatriz[4][3]="3";
        copiaMatriz[5][4]="";
        
        return copiaMatriz;
    }
    /**
     * corrigue un error obtenido por el anterior paso ya que no se hacia un movimiento
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] sieteSegundoPaso(String copiaMatriz[][]){
        copiaMatriz[1][1]="3";
        copiaMatriz[5][3]="3";
        copiaMatriz[1][2]="3";
        copiaMatriz[0][2]="3";
        copiaMatriz[0][3]="3";
        copiaMatriz[0][4]="3";
        copiaMatriz[0][5]="3";
        copiaMatriz[1][5]="3";
        copiaMatriz[2][5]="3";
        copiaMatriz[3][5]="3";
        copiaMatriz[3][4]="3";
        copiaMatriz[3][3]="3";
        copiaMatriz[4][3]="3";
        copiaMatriz[5][3]="";
        
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] ochoPaso(String copiaMatriz[][]){
        copiaMatriz[3][2]="";
        copiaMatriz[3][1]="";
        copiaMatriz[4][1]="";
        copiaMatriz[5][3]="2";
        copiaMatriz[5][4]="2";
        copiaMatriz[5][5]="2";
        
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] nuevePaso(String copiaMatriz[][]){
        copiaMatriz[1][0]="";
        copiaMatriz[2][0]="";
        copiaMatriz[3][1]="1";
        copiaMatriz[3][2]="1";        
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] diezPaso(String copiaMatriz[][]){
        copiaMatriz[2][0]="3";
        copiaMatriz[1][0]="3";
        copiaMatriz[4][3]="";
        copiaMatriz[3][3]="";
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] oncePaso(String copiaMatriz[][]){
        copiaMatriz[3][0]="";
        copiaMatriz[3][1]="";
        copiaMatriz[3][3]="1";
        copiaMatriz[4][3]="1";
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] docePaso(String copiaMatriz[][]){
        copiaMatriz[5][3]="";
        copiaMatriz[5][4]="";
        copiaMatriz[5][5]="";
        copiaMatriz[3][0]="2";
        copiaMatriz[3][1]="2";
        copiaMatriz[4][1]="2";
        return copiaMatriz;
    }
    /**
     * se realiza el paso simulando los movimientos
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    String[][] trecePaso(String copiaMatriz[][]){
        copiaMatriz[3][2]="";
        copiaMatriz[3][3]="";
        copiaMatriz[4][3]="";
        copiaMatriz[5][5]="1";
        copiaMatriz[5][4]="1";
        copiaMatriz[5][3]="1";
        return copiaMatriz;
    }
    /**
     * se imprime en la consola 
     * @param copiaMatriz, se recive la copia de la matriz para ser utilizada
     */
    void imprimir(String copiaMatriz[][]){
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 6; j++) {
                System.out.print(copiaMatriz[i][j]+"\t");
            }
            System.out.println("");
        }
        System.out.println("----------------------------------------------");
    }
}
