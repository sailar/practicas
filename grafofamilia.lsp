(defparameter *nodos* '(
								(abuelosM(Maternos))
								(celerino(70deedad))
								(auxilio(65deedad))
								(abuelosP(Paternos))
								(felipe(80deedad))
								(adriana(74deedad))
								(catalina(madre))
								(isidro(padre))
								(hombres(3))
								(jonny(hermano1))
								(cornelio(hermano2))
								(isidroh(hermano3))
								(mujeres(3))
								(dalia(hermana1))
								(tzitzi(hermana2))
								(Edith(hermana3))
								(yo(♥))
								(cesar(♥))
								(hermanos(6))
								

							 
							
							)
						
)

(defparameter *edges* '
  				((abuelosM (celerino) (auxilio))
   				(abuelosP (felipe) (adriana))
   				(felipe (isidro))
   				(celerino (catalina))
   				(auxilio (catalina))
   				(adriana (isidro))
   				(catalina (hermanos)(yo))
   				(isidro (hermanos)(yo))
   				(hermanos(hombres)(mujeres))
   				(hombres (jonny) (cornelio) (isidroh) )
   				(mujeres (dalia) (tzitzi) (Edith))
   				(yo (cesar))
   				))


(defparameter *max-label-length* 30)

(defun dot-label (exp)
  (if exp
      (let ((s (write-to-string exp :pretty nil)))
	(if (> (length s) *max-label-length*)
	    (concatenate 'string (subseq s 0 (- *max-label-length* 3)) "...")
	  s))
    ""))

(defun dot-name (exp)
(substitute-if #\_ (complement #'alphanumericp) (prin1-to-string exp)))

(defun nodes->dot (nodes)
(mapc (lambda (node)
	(fresh-line)
	(princ (dot-name (car node)))
	(princ "[label=\"")
	(princ (dot-label node))
	(princ "\"];"))
      nodes))


(defun edges->dot (edges)
  (mapc (lambda (node)
	  (mapc (lambda (edge)
		  (fresh-line)
		  (princ (dot-name (car node)))
		  (princ "->")
		  (princ (dot-name (car edge)))
		  (princ "[label=\"")
		  (princ (dot-label (cdr edge)))
		  (princ "\"];"))
		(cdr node)))
	edges))


(defun graph->dot (nodes edges)
  (princ "digraph{")
  (nodes->dot nodes)
  (edges->dot edges)
  (princ "}"))

(defun dot->png (fname thunk)
(with-open-file (*standard-output*
fname
:direction :output
:if-exists :supersede)
(funcall thunk))
(ext:shell (concatenate 'string "dot -Tpng -O " fname)))

(defun dot->png (fname thunk)
  (with-open-file (*standard-output*
		   fname
		   :direction :output
		   :if-exists :supersede)
		  (funcall thunk))
  (ext:shell (concatenate 'string "dot -Tpng -O " fname)))

(with-open-file (my-stream
"testfile.txt"
:direction :output
:if-exists :supersede)
(princ "Hello File!" my-stream))

(defun graph->png (fname nodes edges)
  (dot->png fname
	    (lambda ()
	      (graph->dot nodes edges))))
